package com.carousell.uiautomation.page.impl.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.interfaces.IBrowsePage;

public class AndroidBrowsePage implements IBrowsePage{
	private AndroidDriver<MobileElement> android_driver;;
	
	public AndroidBrowsePage(WebDriver driver) {
		this.android_driver = (AndroidDriver<MobileElement>)driver;
	}
	public void goToCatogory() {
		android_driver.findElementByXPath("//android.widget.TextView[@text='Cars']").click();
		android_driver.findElementByXPath("//android.widget.TextView[@text='For Sale']").click();	 
		android_driver.findElementById("com.thecarousell.Carousell:id/tv_search").click();
		
	}

	public void searchProduct(String productName) {
		 android_driver.findElementById("com.thecarousell.Carousell:id/et_search").sendKeys(productName);
		 android_driver.findElementById("com.thecarousell.Carousell:id/tv_title").click();
		 android_driver.findElementById("com.thecarousell.Carousell:id/btn_search").click();
	}

}
