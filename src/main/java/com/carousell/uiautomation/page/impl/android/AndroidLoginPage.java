package com.carousell.uiautomation.page.impl.android;

import org.openqa.selenium.WebDriver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import com.carousell.uiautomation.page.interfaces.ILoginPage;

public class AndroidLoginPage implements ILoginPage{
	private AndroidDriver<MobileElement> android_driver;;
	
	public AndroidLoginPage(WebDriver driver) {
		this.android_driver = (AndroidDriver<MobileElement>)driver;
	}

	public void doLogin(String username, String password) {
		android_driver.findElementById("com.thecarousell.Carousell:id/welcome_page_login_button").click();
		android_driver.findElementById("com.google.android.gms:id/cancel").click();
		android_driver.findElementByXPath("//android.widget.EditText[@text='email or username']").sendKeys("dk.techi@gmail.com");
		android_driver.findElementByXPath("//android.widget.EditText[@text='password']").sendKeys("cooldk123");
		android_driver.findElementById("com.thecarousell.Carousell:id/login_page_login_button").click();		
	}

}
