package com.carousell.uiautomation.page.impl.android;

import java.util.List;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.interfaces.IProductPage;

public class AndroidProductPage implements IProductPage {

	private AndroidDriver<MobileElement> android_driver;

	public AndroidProductPage(WebDriver driver) {
		this.android_driver = (AndroidDriver<MobileElement>)driver;
	}

	public boolean verifyProduct(String productName) {
		android_driver.findElementById("com.thecarousell.Carousell:id/feature_button").click();
		android_driver.findElementById("com.thecarousell.Carousell:id/feature_button").click();
		List<MobileElement> product_infos = android_driver.findElementsById("com.thecarousell.Carousell:id/tvInfo");
		System.out.println(product_infos.size());
		for (MobileElement product_info : product_infos) {
			if(product_info.getText().toLowerCase().contains(productName.toLowerCase())){
				return true;
			}
		}
		return false;
	}

}
