package com.carousell.uiautomation.page.impl.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.util.List;

import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.interfaces.ISearchResultsPage;

public class AndroidSearchResultsPage implements ISearchResultsPage{

	private AndroidDriver<MobileElement> android_driver;

	public AndroidSearchResultsPage(WebDriver driver) {
		this.android_driver = (AndroidDriver<MobileElement>)driver;
	}

	public void navigateToFirstResult() {
		android_driver.findElementById("com.thecarousell.Carousell:id/feature_button").click();
		 List<MobileElement> card_products = android_driver.findElementsById("com.thecarousell.Carousell:id/card_product");
		 System.out.println(card_products.size());
		 
		 for (MobileElement card_product : card_products) {
			 String text_above_fold = card_product.findElementById("com.thecarousell.Carousell:id/text_above_fold").getText();
			 System.out.println(text_above_fold);
			 if(!text_above_fold.equalsIgnoreCase("Spotlight") && !text_above_fold.equalsIgnoreCase("Promoted")){
				 card_product.click();
				 break;
			 }
		}
		
	}

}
