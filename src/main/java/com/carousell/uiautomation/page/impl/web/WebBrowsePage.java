package com.carousell.uiautomation.page.impl.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.interfaces.IBrowsePage;

public class WebBrowsePage implements IBrowsePage {

	private WebDriver driver;

	public WebBrowsePage(WebDriver driver) {
		this.driver = driver;
	}

	public void goToCatogory() {
		System.out.println("No need to Navigate !");
		
	}

	public void searchProduct(String productName) {
		driver.findElement(By.xpath("/html/body/div[2]/div/header/nav/div/div[2]/form/div/div/div/input")).sendKeys(productName);
		driver.findElement(By.xpath("/html/body/div[2]/div/header/nav/div/div[2]/form/div/div/span[2]/button")).click();
	}

}
