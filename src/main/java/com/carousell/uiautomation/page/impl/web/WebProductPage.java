package com.carousell.uiautomation.page.impl.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.interfaces.IProductPage;

public class WebProductPage implements IProductPage {

	private WebDriver driver;

	public WebProductPage(WebDriver driver) {
		this.driver = driver;
	}

	public boolean verifyProduct(String productName) {
		String productDes = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[1]/div[2]/div[3]/div[1]/div/section/div[2]/section[1]/div[1]/div/div[1]")).getText();
		if(productDes.toLowerCase().contains(productName.toLowerCase())){
			return true;
		}
		return false;
	}

}
