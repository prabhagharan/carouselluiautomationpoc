package com.carousell.uiautomation.page.impl.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.interfaces.ISearchResultsPage;

public class WebSearchResultsPage implements ISearchResultsPage {

	private WebDriver driver;

	public WebSearchResultsPage(WebDriver driver) {
		this.driver = driver;
	}

	public void navigateToFirstResult() {
		driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[1]/div[2]/div[2]/div[5]/div[1]/div[1]/div/figure/div")).click();
	}

}
