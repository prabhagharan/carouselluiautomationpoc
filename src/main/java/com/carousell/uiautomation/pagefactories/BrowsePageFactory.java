package com.carousell.uiautomation.pagefactories;

import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.impl.android.AndroidBrowsePage;
import com.carousell.uiautomation.page.impl.web.WebBrowsePage;
import com.carousell.uiautomation.page.interfaces.IBrowsePage;

public class BrowsePageFactory {
	public static IBrowsePage getPage(String platform, WebDriver driver) {
		if(platform.equals("ANDROID"))
			return new AndroidBrowsePage(driver);
		if(platform.equals("WEB"))
			return new WebBrowsePage(driver);
		else
			return null;
	}
}