package com.carousell.uiautomation.pagefactories;

import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.impl.android.AndroidLoginPage;
import com.carousell.uiautomation.page.impl.web.WebLoginPage;
import com.carousell.uiautomation.page.interfaces.ILoginPage;

public class LoginPageFactory {
	public static ILoginPage getPage(String platform, WebDriver driver) {
		if(platform.equals("ANDROID"))
			return new AndroidLoginPage(driver);
		if(platform.equals("WEB"))
			return new WebLoginPage(driver);
		else
			return null;
	}
}
