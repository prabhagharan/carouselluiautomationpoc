package com.carousell.uiautomation.pagefactories;

import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.impl.android.AndroidProductPage;
import com.carousell.uiautomation.page.impl.web.WebProductPage;
import com.carousell.uiautomation.page.interfaces.IProductPage;

public class ProductPageFactory {

	public static IProductPage getPage(String platform, WebDriver driver) {
		if(platform.equals("ANDROID"))
			return new AndroidProductPage(driver);
		if(platform.equals("WEB"))
			return new WebProductPage(driver);
		else
			return null;
	}

}
