package com.carousell.uiautomation.pagefactories;

import org.openqa.selenium.WebDriver;

import com.carousell.uiautomation.page.impl.android.AndroidSearchResultsPage;
import com.carousell.uiautomation.page.impl.web.WebSearchResultsPage;
import com.carousell.uiautomation.page.interfaces.ISearchResultsPage;

public class SearchResultsPageFactory {

	public static ISearchResultsPage getPage(String platform, WebDriver driver) {
		if(platform.equals("ANDROID"))
			return new AndroidSearchResultsPage(driver);
		if(platform.equals("WEB"))
			return new WebSearchResultsPage(driver);
		else
			return null;
	}

}
