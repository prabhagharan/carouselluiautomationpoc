package com.carousell.uiautomation;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseTest {
	public static WebDriver driver;
	final static String PLATFORM = System.getProperty("platform");
	
	@BeforeSuite
	public void setupAndLoad() throws MalformedURLException {
		if(PLATFORM.equals("ANDROID")){
			createAndroidDriver();
		}else if(PLATFORM.equals("WEB")){
			createFireFoxDriver();
		}
	}

	@AfterSuite
	public void tearDown() throws InterruptedException{
		if(PLATFORM.equals("ANDROID")){
			uninstallApp();
		}if(PLATFORM.equals("WEB")){
			closeDriver();
		}
	}

	private void createAndroidDriver() throws MalformedURLException {
		DesiredCapabilities capabilities;
		final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
		URL url = new URL(URL_STRING);
	    capabilities = new DesiredCapabilities();
	    capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
	    capabilities.setCapability(MobileCapabilityType.APP, "/Users/prabhagd/Documents/Learnings/MobileAutomation/Carousell-2.86.182.170-1236.apk");
	    capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
	    capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
	    capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
	    capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8.0");
	    capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_PACKAGE, "com.thecarousell.Carousell");
	    capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, "com.thecarousell.Carousell.screens.splash.SplashActivity");
	    driver = new AndroidDriver<MobileElement>(url, capabilities);
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	private void createFireFoxDriver() {
		System.setProperty("webdriver.gecko.driver", "/Users/prabhagd/Documents/Learnings/Drivers/geckodriver");
	    driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.get("https://sg.carousell.com/");		
	}
	
	  public void uninstallApp() throws InterruptedException {
		((AndroidDriver<MobileElement>) driver).removeApp("com.thecarousell.Carousell");
		  System.out.println("uninstalled !!!");
	  }
	  
		private void closeDriver() {
			driver.close();
		}

}
