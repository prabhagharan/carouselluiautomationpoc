package com.carousell.uiautomation;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.carousell.uiautomation.page.interfaces.IBrowsePage;
import com.carousell.uiautomation.page.interfaces.ILoginPage;
import com.carousell.uiautomation.page.interfaces.IProductPage;
import com.carousell.uiautomation.page.interfaces.ISearchResultsPage;
import com.carousell.uiautomation.pagefactories.BrowsePageFactory;
import com.carousell.uiautomation.pagefactories.LoginPageFactory;
import com.carousell.uiautomation.pagefactories.ProductPageFactory;
import com.carousell.uiautomation.pagefactories.SearchResultsPageFactory;

public class LoginSearchTest extends BaseTest{
	
	@Test
	public void login_to_app_search_product_test(){
		ILoginPage loginPage = LoginPageFactory.getPage(PLATFORM, driver);
		IBrowsePage browsePage = BrowsePageFactory.getPage(PLATFORM, driver);
		ISearchResultsPage searchResultsPage = SearchResultsPageFactory.getPage(PLATFORM, driver);
		IProductPage productPage = ProductPageFactory.getPage(PLATFORM, driver);
		
		loginPage.doLogin("dk.techi@gmail.com", "cooldk123");
		browsePage.goToCatogory();
		browsePage.searchProduct("Porsche");
		searchResultsPage.navigateToFirstResult();
		Assert.assertTrue(productPage.verifyProduct("Porsche"));
	}

}